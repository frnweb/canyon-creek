<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gFNkBmwbjjBjGWK4xmTDGcTSz/Lr3TqRR+jdP3S+lNN4GBZOT40nukV7X0ocL9sV0Jh+HAnQiT9bqzBnxOGtSA==');
define('SECURE_AUTH_KEY',  'NWaeNhr7hIbSYfIVoGIrdDZpAl1hg5iCTVvFOdcfIl9xEVEG/VWUOaK4vZeAJlDL0XeUUQ6y86yfMawwzRNZbQ==');
define('LOGGED_IN_KEY',    'i00hz4jFQ0bS7OG0EVrCPFix+OXGmh0FdYB0TJVIPKgBcOF9zqxNpCiFFuysFvqZnAAWUbn/DIfsvXJflQtZhQ==');
define('NONCE_KEY',        'ndNkoi1yyxo7pCNF4xC1RAJmpMWBY4o8CmYS4wx3v7axcUo/KypXC47PH/xixZKtk5lklAiQppamQQseyWZBbg==');
define('AUTH_SALT',        'prPARDVhyepPCmQtjuRhgeiCt1BY9gQCQmu6mqe3bKhZXD0VUS6bEw3DHFp0zii4dpzzL/crVy6QDaECTxzTIQ==');
define('SECURE_AUTH_SALT', 'Tb3rZiZAG6mURkiahbbMXXuqDZQYuOWy7FZWnNLigqnvCmfLUgWR9fb6wB559QXvm9ivRAjQ5DEy+/Aye9WPcg==');
define('LOGGED_IN_SALT',   'Im+v1+/H1XtQc3kgObGgUjN2PIMleE/mkA/NPVjLwGxkHwXplK2BS/ocE3myM+S9BMmCnpXQek4wCKZ9jvjglw==');
define('NONCE_SALT',       '+IYHtvyjFygG67Q3QQLkZ7Jk1raXd9lGNdyRob/xyR12lRBjDgm20nBI2Q3+z5PkD2Fpa/BUecx1c1H/dFf8xQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

// This enables debugging.
define( 'WP_DEBUG', true );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
