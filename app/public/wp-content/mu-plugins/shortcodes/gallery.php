<?php
// GALLERY Dependent on Slick Carousel https://kenwheeler.github.io/slick/
	function sl_gallery( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'class'		=> ''
		), $atts );
		return '<div class="sl_gallery sl_gallery--' . esc_attr($specs['class'] ) . '">' . do_shortcode ( $content ) . '</div>';
	}
	add_shortcode ('gallery', 'sl_gallery' );
///GALLERY

// GALLERY IMAGE
	function sl_gallery_image ( $atts ) {
		$specs = shortcode_atts( array(
			'src'		=> ''
			), $atts );

		return '<div class="sl_gallery__image"  style="background-image: url('. esc_attr($specs['src'] ) .'"></div>';

	}

	add_shortcode ('gallery-image', 'sl_gallery_image' );
///GALLERY IMAGE
?>
