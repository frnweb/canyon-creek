<?php
// DIVIDER
	function sl_divider ( $atts) {
	    $specs = shortcode_atts( array(
	        'class'     => '',
			), $atts );
	    return '<div class="sl_divider sl_divider--' . esc_attr($specs['class']) . ' "></div>';
	}
	add_shortcode( 'divider', 'sl_divider' );
///DIVIDER
?>