<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

/**
 * Initialize ACF Builder
 */
add_action('init', function () {
    collect(glob(get_stylesheet_directory() . '/app/fields/**/*.php'))->map(function ($field) {
        return require_once($field);
    })->map(function ($field) {
        if ($field instanceof FieldsBuilder) {
            acf_add_local_field_group($field->build());
        }
    });
});