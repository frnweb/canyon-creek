<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$optionsglobal = new FieldsBuilder('global_options');

$optionsglobal
    ->setLocation('options_page', '==', 'theme-general-settings')

    //Address Fields
    ->addGroup('address', [
         'wrapper' => ['width' => 50]
    ])      
        ->addText('location_name', [
            'label' => 'Location Name',
            'ui' => $config->ui
        ])
        ->setInstructions('Name of location or facility.')

        ->addText('street_address', [
            'label' => 'Street Address',
            'ui' => $config->ui
        ])
        ->setInstructions('The street address for the facility. This will be used throughout the Site.')

        ->addText('city', [
            'label' => 'City',
            'ui' => $config->ui
        ])
        ->setInstructions('Put the City i.e. Nashville')

        ->addText('state', [
            'label' => 'State',
            'ui' => $config->ui
        ])
        ->setInstructions('Put the State here i.e. TN')

        ->addText('zip_code', [
            'label' => 'Zip Code',
            'ui' => $config->ui
        ])
        ->setInstructions('The Zip Code of the facility')

        //Google Map
        ->addGoogleMap('google_map')

    ->endGroup()

    //Phone Numbers
    ->addGroup('phone', [
        'wrapper' => ['width' => 50]
    ])
        ->addText('main', [
            'label' => 'Global Phone Number',
            'ui' => $config->ui
        ])
        ->addText('sms', [
            'label' => 'SMS number',
            'ui' => $config->ui
        ])
    ->setInstructions('Add the number for the SMS messenging.')
    ->endGroup()

    // GLOBAL CTA
    ->addGroup('global_cta', [
        'label' => 'Global CTA'
      ])
    ->setInstructions('This is the global CTA that will appear at the bottom of all default page templates')
    ->addWysiwyg('wysiwyg', [
        'label' => 'Content',
        'ui' => $config->ui
    ])
    //Button
    ->addFields(get_field_partial('modules.button'))
    ->endGroup();

return $optionsglobal;