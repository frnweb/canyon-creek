<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 100],
];

$wrapper_open = new FieldsBuilder('wrapper_open');

$wrapper_open
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$wrapper_open
	->addTab('optional_content', ['placement' => 'left'])
    //PreHeader
		->addTrueFalse('check_box', [
			'label' => 'Wrapper Header?',
			'wrapper' => ['width' => 30]
		])
		->setInstructions('Optional header for the wrapper')
		
		->addText('header', [
			'label' => 'Header',
			'wrapper' => ['width' => 70]
		])
		->conditional('check_box', '==', 1 );
    
return $wrapper_open;